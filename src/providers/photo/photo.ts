// import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

import { Camera, CameraOptions } from '@ionic-native/camera';
/*
  Generated class for the PhotoProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class PhotoProvider {

public cameraImage : string;

  constructor(public http: Http, 
                    private camera : Camera                             
                    ) {
    console.log('Hello PhotoProvider Provider');
  }


   /**
     *
     * Select an image from the device photo library
     *
     * @public
     * @method selectImage
     * @return {Promise}
     */
  selectImage () : Promise<any> 
  {
    return new Promise(resolve => 
  {
    let cameraOptions : CameraOptions = {
        sourceType         : this.camera.PictureSourceType.PHOTOLIBRARY,
        destinationType    : this.camera.DestinationType.DATA_URL,
        quality            : 100,
        targetWidth        : 320,
        targetHeight       : 240,
        encodingType       : this.camera.EncodingType.JPEG,
        correctOrientation : true
   

    };
    this.camera.getPicture(cameraOptions)
    .then((data) =>{
      this.cameraImage = "data:image/jpeg:base64." + data;
      resolve(this.cameraImage); 
    });
  });
  }
}
