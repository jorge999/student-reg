import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Lower6Page } from './lower6';

@NgModule({
  declarations: [
    Lower6Page,
  ],
  imports: [
    IonicPageModule.forChild(Lower6Page),
  ],
})
export class Lower6PageModule {}
