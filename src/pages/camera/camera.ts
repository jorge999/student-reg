import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
// import { Camera, CameraOptions } from '@ionic-native/camera';
// import { Crop } from '@ionic-native/crop';
//import {ImageCropperComponent, CropperSettings, Bounds} from 'ng2-img-cropper';
import { PhotoProvider } from '../../providers/photo/photo';

/**
 * Generated class for the CameraPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-camera',
  templateUrl: 'camera.html',
})
export class CameraPage {
  // base64Image: string;

    //Access the image cropper DOM element
    //@ViewChild('cropper') ImageCropper : ImageCropperComponent;

    //Object for storing - and eventually initialising - image cropper settings
   public cropperSettings;
    //Will set the cropped width for the image
    public croppedWidth : Number;
    //Will set the cropped height for the image
   public croppedHeight : Number;
    //Object for storing image data
    public data : any;
    //Determines whether the Save Image button is to be displayed or not
    public canSave : boolean = false;

  constructor(public navCtrl: NavController, 
    public navParams: NavParams, 
    private photo : PhotoProvider
  ) {
          // // Here we set up the Image Cropper component settings
          // this.cropperSettings = new CropperSettings();

          // // Hide the default file input for image selection (we'll be
          // // using the Camera plugin instead)
          // this.cropperSettings.noFileInput = true;
    
          // // Create a new cropped image object when the cropping tool
          // // is resized
          // this.cropperSettings.cropOnResize = true;
    
          // // We want to convert the file type for a cropped image to a
          // // JPEG format
          // this.cropperSettings.fileType = 'image/jpeg';
    
          // // We want to be able to adjust the size of the cropping tool
          // // by dragging from any corner in any direction
          // this.cropperSettings.keepAspect = false;
    
          // // Create an object to store image related cropping data
          // this.data = {};
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CameraPage');
  }


  /**
     *
     * Determine the width & height for the cropped image (and enable the
     * Save Image button)
     *
     * @public
     * @method handleCropping
     * @param bounds              {Bounds}      Capture the component's crop event
                                                (and subsequent properties)
     * @return none
     */
  //   handleCropping(bounds : Bounds)
  //   {
  //      this.croppedHeight 	= bounds.bottom  -  bounds.top;
  //      this.croppedWidth 		= bounds.right   -  bounds.left;
  //      this.canSave            = true;
  //   }

  //   /**
  //    *
  //    * Select an image from the device Photo Library
  //    *
  //    * @public
  //    * @method selectImage
  //    * @return none
  //    */
  //  selectImage()
  //  {
  //     this.canSave = false;
  //     this.photo.selectImage()
  //     .then((data : any) =>
  //     {
  //        // Create an Image object, assign retrieved base64 image from
  //        // the device photo library
  //        let image : any  = new Image();
  //        image.src	= data;


  //        // Assign the Image object to the ImageCropper component
  //        this.ImageCropper.setImage(image);
  //     })
  //     .catch((error : any) =>
  //     {
  //        console.dir(error);
  //     });
  //  }

      /**
     *
     * Retrieve the cropped image value (base64 image data)
     *
     * @public
     * @method saveImage
     * @return none
     */
    
    saveImage()
    {
       console.dir(this.data.image);
    }
 



















  // async takePicture(): Promise<any> { 
  //   try{ 
  //       this.image = await this.camera.getPicture(this.options);
  //   }
  // catch(err){
  //   console.log(err);
  // }
  // takePicture() {
  //   const options: CameraOptions = {
  //     quality: 80,
  //     destinationType: this.camera.DestinationType.DATA_URL,
  //     encodingType: this.camera.EncodingType.JPEG,
  //     // mediaType: this.camera.MediaType.PICTURE,
  //     sourceType: this.camera.PictureSourceType.CAMERA,
  //     allowEdit: true,
  //     targetWidth: 1000,
  //     targetHeight: 1000,
  //     saveToPhotoAlbum: true
  //   };

  //   this.camera.getPicture(options).then((imageData) => {
  //     // imageData is either a base64 encoded string or a file URI
  //     // If it's base64:
  //     this.base64Image = 'data:image/jpeg;base64,' + imageData;
  //   }, (err) => {
  //     console.log(err);
  //     // Handle error
  //   });
  // }

  // cropPic(){
  //    this.crop.crop('path/to/image.jpg', {quality: 75})
  // .then(
  //   newImage => console.log('new image path is: ' + newImage),
  //   error => console.error('Error cropping image', error)
  // );
  // }
 
}
