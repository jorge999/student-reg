import { Component, ViewChild, ElementRef } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { Crop } from '@ionic-native/crop';
import { File } from '@ionic-native/file';

// import { Platform } from 'ionic-angular';
// import Cropper from 'cropperjs';

/**
 * Generated class for the FormPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-form',
  templateUrl: 'form.html',
})
export class FormPage {
  
  @ViewChild('imageSrc') imageElement: ElementRef;
  // base64Image: string;
  imageUrl: string = '';
  // cropperInstance: Cropper;
  urlImg: any;


  constructor( private file: File, private crop: Crop, public navCtrl: NavController, public navParams: NavParams, private camera: Camera) {
   
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FormPage');
  }

  logout(){
    this.navCtrl.setRoot('LoginPage');
    console.log('logout success');
  }


  takePicture() {

    const options: CameraOptions = {
      quality: 100,
      destinationType: this.camera.DestinationType.FILE_URI,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      sourceType: this.camera.PictureSourceType.CAMERA,
      allowEdit: false,
      targetWidth: 3000,
      targetHeight: 3000,
      saveToPhotoAlbum: true,
      correctOrientation: true

    };

    this.camera.getPicture(options).then((imageData) => {
      // imageData is either a base64 encoded string or a file URI
      // If it's base64:
      this.imageUrl = imageData;
      this.cropPic();
    }, (err) => {
      console.log(err);
      // Handle error
    });
  }
  name: string;
  nric: string;
  form: string; 
  clas: string;
  image: string;
  cropImg: string;

 test: string;

  cropPic(){

    
    this.crop.crop(this.imageUrl, {quality: 100})
  .then((newImage) => {
    this.urlImg = newImage;
    console.log('new image path is: ' + newImage)},
  (error) => {
    console.error('Error cropping image', error)
    // return newImage;
  
  });
  }

Save(name, nric, form, clas, image, cropImg){
 this.name = name;
 this.nric = nric;
 this.form = form; 
 this.clas = clas;
 this.image = image;
 this.cropImg = cropImg;

  console.log("name : ", this.name,  "nric : ",  this.nric + "class : ", this.clas, "form : ", this.form, "image : ", this.image, "cropImg : ", this.cropImg );
}

// testing(name, nric, form, clas){
//   this.name = name;
//   this.nric = nric;
//   this.form = form; 
//   this.clas = clas;
//   console.log("test : ", this.name, "nric : ", this.nric, "class : ", this.clas,  "form : ", this.form);
// }
}

  

