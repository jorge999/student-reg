import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ImageEditPage } from './image-edit';

@NgModule({
  declarations: [
    ImageEditPage,
  ],
  imports: [
    IonicPageModule.forChild(ImageEditPage),
  ],
})
export class ImageEditPageModule {}
