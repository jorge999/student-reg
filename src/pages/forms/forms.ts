import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the FormsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-forms',
  templateUrl: 'forms.html',
})
export class FormsPage {
  post: any;
  form: any=[];

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.form = navParams.get('post');
  }

  ionViewDidLoad() {
    console.log('--->', this.form);
    console.log('ionViewDidLoad FormsPage');
  }

}
