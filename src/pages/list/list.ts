import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the ListPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-list',
  templateUrl: 'list.html',
})
export class ListPage {
//   forms = [
// 'Form 1',
// 'Form 2',
// 'Form 3',
// 'Form 4',
// 'Form 5',
// 'Lower 6',
// 'Upper 6'
//   ];

  constructor(public navCtrl: NavController, public navParams: NavParams) {


  }


// initializeItems() {
  //    this.forms = [

  //   {FormPage: 'Form1Page', FormName:'Form 1'},
  //   {FormPage: 'Form2Page', FormName:'Form 2'},
  //   {FormPage: 'Form3Page', FormName:'Form 3'},
  //   {FormPage: 'Form4Page', FormName:'Form 4'},
  //   {FormPage: 'Form5Page', FormName:'Form 5'},
  //   {FormPage: 'LowerPage', FormName:'Lower 6'},
  //   {FormPage: 'UpperPage', FormName:'Upper 6'}
    
  //     ];
  // }
 
//   getItems(ev) {
//     // Reset items back to all of the forms
//     this.initializeItems();

//     // set val to the value of the searchbar
//     let val = ev.target.value;

//     // if the value is an empty string don't filter the items
//     if (val && val.trim() != '') {
//         this.forms = this.forms.filter((form) => {
//             return (form.CalcName.toLowerCase().indexOf(val.toLowerCase()) > -1);
//         })
//     }
// }


// itemSelected(form: any):void {
//     console.log("Selected Item", form.FormPage);
//     this.navCtrl.push(form.FormPage);
//   }


  // itemSelected(form: string) {
  //   console.log("Selected Item", {form});
  //   this.navCtrl.push('FormsPage', {form});
  // }

  home() {
    this.navCtrl.setRoot('HomePage');
  }

  form1(){
    this.navCtrl.push('Form1Page');
    console.log('form 1 clicked');
  }
  form2(){
    this.navCtrl.push('Form2Page');
    console.log('form 1 clicked');
  }
  form3(){
    this.navCtrl.push('Form3Page');
    console.log('form 1 clicked');
  }
  form4(){
    this.navCtrl.push('Form4Page');
    console.log('form 1 clicked');
  }
  form5(){
    this.navCtrl.push('Form5Page');
    console.log('form 1 clicked');
  }
  lower6(){
    this.navCtrl.push('Lower6Page');
    console.log('form 1 clicked');
  }
  upper6(){
    this.navCtrl.push('Upper6Page');
    console.log('form 1 clicked');
  }
}
