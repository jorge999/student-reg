import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the Form5Page page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-form5',
  templateUrl: 'form5.html',
})
export class Form5Page {
  classes = [
    'Class A',
    'Class B',
    'Class C',
    'Class D',
    'Class F'
      ];
  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Form5Page');
  }

  classSelected(item: string){
    console.log ("Selected class", item);
  }
  
}
