import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Upper6Page } from './upper6';

@NgModule({
  declarations: [
    Upper6Page,
  ],
  imports: [
    IonicPageModule.forChild(Upper6Page),
  ],
})
export class Upper6PageModule {}
