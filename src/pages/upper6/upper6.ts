import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the Upper6Page page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-upper6',
  templateUrl: 'upper6.html',
})
export class Upper6Page {
  classes = [
    'Class A',
    'Class B',
    'Class C',
    'Class D'
      ];
  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Upper6Page');
  }

  classSelected(item: string){
    console.log ("Selected class", item);
  }
}
