import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { Camera } from '@ionic-native/camera';
import { MyApp } from './app.component';
import { ImageCropperModule } from "ng2-img-cropper/index";
import { Crop } from '@ionic-native/crop';
import { File } from '@ionic-native/file';
import { SQLite } from '@ionic-native/sqlite';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { PhotoProvider } from '../providers/photo/photo';
import { RegserviceProvider } from '../providers/regservice/regservice';

@NgModule({
  declarations: [
    MyApp

  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp
 
  ],
  providers: [
    StatusBar,
    SplashScreen,
    Camera,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    PhotoProvider,
    Crop,
    File,
    SQLite,
    RegserviceProvider
    
  ]
})
export class AppModule {}
